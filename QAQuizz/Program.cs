﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QAQuizz
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input a word to pluralize: ");
            var word = Console.ReadLine();

            var sample = new UnitTestSample();
            var plural = sample.MakePlural(word);

            Console.WriteLine("The plural of {0} is {1}", word, plural);
            Console.WriteLine("Press any key to continue...");
            Console.Read();
        }
    }
}
