﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QAQuizz
{
    public class UnitTestSample
    {
        /// <summary>
        /// Takes a word and pluralizes it depending on it's ending character.
        /// </summary>
        /// <param name="input">The Word to pluralize</param>
        /// <returns></returns>
        public string MakePlural(string input)
        {
            if (input.EndsWith("y"))
            {
                return input.Substring(0, input.Length - 1) + "ies";
            }

            if (input.EndsWith("s"))
            {
                return input + "es";
            }

            return input + "s";
        }
    }
}
