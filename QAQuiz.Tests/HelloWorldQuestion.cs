﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QAQuiz.Tests
{
    public class HelloWorldQuestion
    {
        //Requirement:
        //Write a method called HelloWorld that takes a string x and returns a string
        //If x is an integer and that integer is evenly divisible by 3 return the string "Hello"
        //If x is an integer and that integer is evenly divisible by 5 return the string "World"
        //If x is an integer and that integer is evenly divisible by 3 AND 5 return the string "HelloWorld"
        //Otherwise return the input x

        //Once you have completed this method, write test cases in a text file called "HelloWorldTestCases"
        //Write BDD Acceptance tests using SpecFlow in the QAQuiz.Tests to ensure all test cases are satisfied
        // ** This should be in the format of:
        // Given...
        // When...
        // Then...
    }
}
